//
//  GoProNetworking.swift
//  GoInstaPro
//
//  Created by Wade Sellers on 6/21/16.
//  Copyright © 2016 Wade Sellers. All rights reserved.
//

/*
To ensure proper JSON navigation, these are the print statements we can use to find data we care about
1) print("ImageURL: \(result["images"]["standard_resolution"]["url"])")
2) print("Comments: \(result["comments"]["count"].stringValue)")
3) print("Likes: \(result["likes"]["count"].stringValue)")
*/

import UIKit
import Alamofire
import SwiftyJSON

class GoProNetworking: NSObject {

	static let goProUrl = "https://www.instagram.com/gopro/media/"

	class func getGoProData(_ completionHandler : @escaping ((_ imageItemArray:[ImageItem]) -> Void)) -> [ImageItem] {

		let queue = DispatchQueue(label: "com.WadeSellers.GoInstaPro.background", attributes: DispatchQueue.Attributes.concurrent)

		var imageItemArray:[ImageItem] = [ImageItem]()

		let request = Alamofire.request(goProUrl, parameters: nil)
		request.responseJSON(queue: queue, options: .allowFragments, completionHandler: { response in
			switch response.result {
			case .success:
				if let value = response.result.value {
					let json = JSON(value)
					for result in json["items"].arrayValue {
						let imageItem = ImageItem(json: result)
						imageItemArray.append(imageItem)
					}
					return completionHandler(imageItemArray)
				}
			case .failure(let error):
				print("Error when retrieving GoPro info: \(error)")
			}
		})

		return imageItemArray
	}
	
}
