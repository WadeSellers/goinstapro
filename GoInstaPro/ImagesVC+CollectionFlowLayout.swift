//
//  ImagesVC+CollectionFlowLayout.swift
//  GoInstaPro
//
//  Created by Wade Sellers on 6/21/16.
//  Copyright © 2016 Wade Sellers. All rights reserved.
//

import Foundation
import UIKit

extension ImagesViewController {

	// Collection view flow layout setup

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
		let screenSize = UIScreen.main.bounds
		let screenWidth = screenSize.width
		let cellSquareSize: CGFloat = screenWidth / 2.0
		return CGSize(width: cellSquareSize, height: cellSquareSize);
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
		return UIEdgeInsetsMake(0, 0, 0.0, 0.0)
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
		return 0.0
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
		return 0.0
	}

}
